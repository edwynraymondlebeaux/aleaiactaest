<?php

namespace AppBundle\Repository;

/**
 * ArticleRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class ArticleRepository extends \Doctrine\ORM\EntityRepository
{
    public function findNLatestArticles($n = 5, $showOnlyPublic = false)
    {
        $queryBuilder = $this->createQueryBuilder('a');

        if ($showOnlyPublic) {
            $queryBuilder->andWhere('a.isPublic = :isPublic')
                ->setParameter('isPublic', $showOnlyPublic);
        }

        $queryBuilder->orderBy('a.addedTime', 'desc')
                ->setMaxResults($n);

        $query = $queryBuilder->getQuery();

        return $query->getResult();
    }

    public function findNLatestArticlesByUser($userId, $n = 5, $showOnlyPublic = false)
    {
        $queryBuilder = $this->createQueryBuilder('a');

        if ($showOnlyPublic) {
            $queryBuilder->andWhere('a.isPublic = :isPublic')
                ->setParameter('isPublic', $showOnlyPublic);
        }

        $queryBuilder->andWhere('a.addedBy = :userId')
            ->setParameter('userId', $userId)
            ->orderBy('a.addedTime', 'desc')
            ->setMaxResults($n);

        $query = $queryBuilder->getQuery();

        return $query->getResult();
    }

    public function findAllArticlesOrderedByTime($showOnlyPublic = false)
    {
        $queryBuilder = $this->createQueryBuilder('a');
            
        if ($showOnlyPublic) {
            $queryBuilder->andWhere('a.isPublic = :isPublic')
                ->setParameter('isPublic', $showOnlyPublic);
        }

        $queryBuilder->orderBy('a.addedTime', 'desc');

        $query = $queryBuilder->getQuery();

        return $query->getResult();
    }
}
