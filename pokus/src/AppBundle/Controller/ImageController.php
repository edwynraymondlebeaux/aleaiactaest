<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Article;
use AppBundle\Entity\Image;
//use AppBundle\Entity\Location;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
//use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
//use Symfony\Component\Form\Extension\Core\Type\DateType;
//use Symfony\Component\Form\Extension\Core\Type\FileType;
//use Symfony\Component\Form\Extension\Core\Type\SubmitType;
//use Symfony\Component\Form\Extension\Core\Type\TextType;
//use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
//use Symfony\Component\Validator\Constraints\DateTime;
//use Symfony\Component\Validator\Constraints\File;
//use Symfony\Component\Validator\Constraints\All as AllConstraint;
//use AppBundle\Entity\Tag;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Doctrine\ORM\EntityRepository;
//use AppBundle\Entity\Comment;

class ImageController extends Controller
{
    /**
     * @Route("/image/list",
     *     name="image_list"
     * )
     *
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function listAction(Request $request)
    {
        $images = $this->getDoctrine()->getRepository('AppBundle:Image')->findAll();

        return $this->render('image/list.html.twig', array(
            'images' => $images,
        ));
    }

    /**
     * @Route("/image/delete",
     *     name="delete_image"
     * )
     *
     * @Security("has_role('ROLE_USER')")
     */
    public function deleteImageAction(Request $request)
    {
        $data = array('result' => false);

        $imageId = $request->request->getInt('imageId', null);
        $userId = $request->request->getInt('userId', null);

        if ($this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            $currentUser = $this->getUser();
            if (($currentUser->getId() == $userId) || ($this->get('security.authorization_checker')->isGranted('ROLE_ADMIN'))){

                $image = $this->getDoctrine()->getRepository('AppBundle:Image')->findOneBy(array(
                    'id' => $imageId,
                ));

                $em = $this->getDoctrine()->getEntityManager();

                $em->remove($image);
                $em->flush();
                $data = array(
                    'result' => true,
                    'id' => $imageId,
                );
            }
        }

        $response = new JsonResponse();
        $response->setData($data);
        return $response;
    }

}