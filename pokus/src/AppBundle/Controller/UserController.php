<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Article;
use AppBundle\Entity\Image;
//use AppBundle\Entity\Location;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
//use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
//use Symfony\Component\Form\Extension\Core\Type\DateType;
//use Symfony\Component\Form\Extension\Core\Type\FileType;
//use Symfony\Component\Form\Extension\Core\Type\SubmitType;
//use Symfony\Component\Form\Extension\Core\Type\TextType;
//use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
//use Symfony\Component\Validator\Constraints\DateTime;
//use Symfony\Component\Validator\Constraints\File;
//use Symfony\Component\Validator\Constraints\All as AllConstraint;
//use AppBundle\Entity\Tag;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Doctrine\ORM\EntityRepository;
//use AppBundle\Entity\Comment;

class UserController extends Controller
{
    /**
     * @Route("/user/list",
     *     name="user_list"
     * )
     *
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function listAction(Request $request)
    {
        $users = $this->getDoctrine()->getRepository('AppBundle:User')->findAll();

        return $this->render('user/list.html.twig', array(
            'users' => $users,
        ));
    }

    /**
     * @Route("/user/enable",
     *     name="enable_user"
     * )
     *
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function enableAction(Request $request)
    {
//        $data = array('result' => false);

        $requestUserId = $request->request->getInt('currentUserId', null);
        $userId = $request->request->getInt('userId', null);
        $newStatus = ! $request->request->getBoolean('currentStatus', null);

        if ($this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            $currentUser = $this->getUser();
            if (($currentUser->getId() == $requestUserId) || ($this->get('security.authorization_checker')->isGranted('ROLE_ADMIN'))){

                $user = $this->getDoctrine()->getRepository('AppBundle:User')->findOneBy(array(
                    'id' => $userId,
                ));

                $user->setEnabled($newStatus);

                $this->get('fos_user.user_manager')->updateUser($user);

//                $data = array(
//                    'result' => true,
//                    'id' => $userId,
//                    'newStatus' => $newStatus,
//                    'user' => $user,
//                );
            }
        }

//        $response = new JsonResponse();
//        $response->setData($data);
//        return $response;

        $users = $this->getDoctrine()->getRepository('AppBundle:User')->findAll();

        return $this->render('user/list.html.twig', array(
            'users' => $users,
        ));
    }

}