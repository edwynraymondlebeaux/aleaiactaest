<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Article;
//use AppBundle\Entity\Image;
//use AppBundle\Entity\Location;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
//use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
//use Symfony\Component\Form\Extension\Core\Type\DateType;
//use Symfony\Component\Form\Extension\Core\Type\FileType;
//use Symfony\Component\Form\Extension\Core\Type\SubmitType;
//use Symfony\Component\Form\Extension\Core\Type\TextType;
//use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
//use Symfony\Component\Validator\Constraints\DateTime;
//use Symfony\Component\Validator\Constraints\File;
//use Symfony\Component\Validator\Constraints\All as AllConstraint;
use AppBundle\Entity\Tag;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Doctrine\ORM\EntityRepository;
//use AppBundle\Entity\Comment;


class TagController extends Controller
{
    /**
     * @Route("/tag/list", name="tag_list")
     */
    public function listAction(Request $request)
    {
        $tags = $this->getDoctrine()->getRepository('AppBundle:Tag')->findAll();

        return $this->render('tag/list.html.twig', array(
            'tags' => $tags,
        ));
    }

    /**
     * @Route("/tag/detail/{id}",
     *     name="tag_detail",
     *     requirements={
     *         "id"="\d+"
     *     }
     * )
     */
    public function detailAction(Request $request, $id)
    {
        $tag = $this->getDoctrine()->getRepository('AppBundle:Tag')->findOneBy(array(
            'id' => $id,
        ));

        return $this->render('tag/detail.html.twig', array(
            'tag' => $tag,
        ));
    }

    /**
     * @Route("/tag/delete-tag",
     *     name="delete_tag"
     * )
     *
     * @Security("has_role('ROLE_USER')")
     */
    public function deleteImageAction(Request $request)
    {
        $data = array('result' => false);

        $tagId = $request->request->getInt('tagId', null);
        $userId = $request->request->getInt('userId', null);

        if ($this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            $currentUser = $this->getUser();
            if (($currentUser->getId() == $userId) || ($this->get('security.authorization_checker')->isGranted('ROLE_ADMIN'))){

                $tag = $this->getDoctrine()->getRepository('AppBundle:Tag')->findOneBy(array(
                    'id' => $tagId,
                ));

                $em = $this->getDoctrine()->getEntityManager();

                $em->remove($tag);
                $em->flush();
                $data = array(
                    'result' => true,
                    'id' => $tagId,
                );
            }
        }

        $response = new JsonResponse();
        $response->setData($data);
        return $response;
    }
}

