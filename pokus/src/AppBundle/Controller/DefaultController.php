<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        if ($this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            $myArticles = $this->getDoctrine()->getRepository('AppBundle:Article')->findNLatestArticlesByUser($this->getUser()->getId(), 5);
            $articles = $this->getDoctrine()->getRepository('AppBundle:Article')->findNLatestArticles(5);
        } else {
            $myArticles = [];
            $articles = $this->getDoctrine()->getRepository('AppBundle:Article')->findNLatestArticles(5, true);
        }
        return $this->render('default/index.html.twig', [
            'articles' => $articles,
            'myArticles' => $myArticles,
        ]);
    }
}
