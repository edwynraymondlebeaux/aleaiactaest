<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Article;
use AppBundle\Entity\Image;
use AppBundle\Entity\Location;
use Parsedown;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints\DateTime;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\Validator\Constraints\All as AllConstraint;
use AppBundle\Entity\Tag;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Doctrine\ORM\EntityRepository;
use AppBundle\Entity\Comment;

class ArticleController extends Controller
{
    /**
     * @Route("/article/add", name="article_add")
     *
     * @Security("has_role('ROLE_USER')")
     */
    public function addAction(Request $request)
    {
        $article = new Article();

        $form = $this->createFormBuilder($article)
            ->add('title', TextType::class)
            ->add('text', TextareaType::class)
            ->add('isPublic', CheckboxType::class, array(
                'label'    => 'Zobraziť tento článok verejne?',
                'required' => false,
            ))
            ->add('images', FileType::class, array(
                'mapped' => false,
                'required' => false,
                'constraints' => array(
                    new AllConstraint(array(
                        new File(array(
                            'maxSize' => '1024k',
                            'mimeTypes' => array(
                                'image/jpeg',
                                'image/gif',
                                'image/png',
                            ),
                            'mimeTypesMessage' => 'Prosím, zvoľte validný obrázok (jpg, png, gif)',
                        ))
                    ))
                ),
                'data_class' => null,
                'multiple' => true,
            ))
            ->add('tags', TextType::class, array(
                'mapped' => false,
                'required' => false,
                'attr' => array(
                    'class' => 'tokenfield',
                )
            ))
            ->add('locations', EntityType::class, array(
                'class' => 'AppBundle:Location',
                'query_builder' => function(EntityRepository $er) {
                    return $er->createQueryBuilder('l')
                        ->andWhere('l.name != :name')
                        ->setParameter('name', 'NULL')
                        ->orderBy('l.name', 'ASC');
                },
                'choice_label' => 'name',
                'required' => false,
                'multiple' => true,
            ))
            ->add('add', SubmitType::class, array('label' => 'Pridaj článok'))
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $em = $this->getDoctrine()->getManager();
            $tagRepository = $this->getDoctrine()->getRepository('AppBundle:Tag');

            $article->setAddedBy($this->getUser());
            $now = new \DateTime();
            $article->setAddedTime($now);

            $em->persist($article);

            $uploadedImages = $form['images']->getData();

            if ($uploadedImages && !empty($uploadedImages[0])) {
                foreach ($uploadedImages as $uploadedImage) {
                    $filename = uniqid() . '.' . $uploadedImage->guessExtension();
                    $path = $this->container->getParameter('dir_images');

                    $image = new Image();
                    $image->setMime($uploadedImage->getMimeType())
                        ->setTitle($uploadedImage->getClientOriginalName())
                        ->setAddedTime($now)
                        ->setPath($path . '/' . $filename)
                        ->setUrl($this->container->getParameter('url_dir_images') .'/' . $filename);

                    $uploadedImage->move($path, $filename);

                    $info = getimagesize($path . '/' . $filename);

                    $image->setWidth($info[0])
                        ->setHeight($info[1]);

                    $article->addImage($image);

                    $em->persist($image);
                }
            }

            $tagList = $form['tags']->getData();
            if ($tagList) {
                $tagList = explode(',', $tagList);

                foreach ($tagList as $item) {
                    $item = trim($item);

                    $tag = null;
                    if (!is_numeric($item)) {
                        $tag = new Tag();
                        $tag->setName($item);
                        $em->persist($tag);
                    } else if ($item) {
                        $tag = $tagRepository->findOneById($item);
                    }

                    if ($tag) {
                        $article->addTag($tag);
                    }
                }
            }

            $em->flush();

            return $this->redirectToRoute('article_detail', array("id" => $article->getId()));
        }

        $allTags = $this->getDoctrine()->getRepository('AppBundle:Tag')->findAll();

        return $this->render('article/add.html.twig', array(
            'form' => $form->createView(),
            'allTags' => $allTags,
        ));
    }

    /**
     * @Route("/article/list", name="article_list")
     */
    public function listAction(Request $request)
    {
        if ($this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            #$articles = $this->getDoctrine()->getRepository('AppBundle:Article')->findAll();
            $articles = $this->getDoctrine()->getRepository('AppBundle:Article')->findAllArticlesOrderedByTime(false);
        } else {
            $articles = $this->getDoctrine()->getRepository('AppBundle:Article')->findAllArticlesOrderedByTime(true);
        }

        return $this->render('article/list.html.twig', array(
            'articles' => $articles,
        ));
    }

    /**
     * @Route("/article/detail/{id}",
     *     name="article_detail",
     *     requirements={
     *         "id"="\d+"
     *     }
     * )
     */
    public function detailAction(Request $request, $id)
    {
        $article = $this->getDoctrine()->getRepository('AppBundle:Article')->findOneBy(array(
            'id' => $id,
        ));

        if (!$article->getIsPublic()) {
            $this->denyAccessUnlessGranted('ROLE_USER', null, 'Unable to access this page!');
        }

        $comment = new Comment();

        $form = $this->createFormBuilder($comment)
            ->add('text', TextareaType::class, array(
                'label' => 'Pridaj komentár:',
            ))
            ->add('save', SubmitType::class, array('label' => 'Pridaj'))
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $comment->setAddedBy($this->getUser());
            $comment->setArticle($article);
            $now = new \DateTime();
            $comment->setAddedTime($now);

            $em = $this->getDoctrine()->getManager();
            $em->persist($comment);
            $em->flush();

            return $this->redirectToRoute('article_detail', array("id" => $article->getId()));
        }

        return $this->render('article/detail.html.twig', array(
            'article' => $article,
            'form' => $form->createView(),
            'parsedown' => Parsedown::instance()->setMarkupEscaped(true),
        ));
    }

    /**
     * @Route("/article/edit/{id}",
     *     name="article_edit",
     *     requirements={
     *         "id"="\d+"
     *     }
     * )
     *
     * @Security("has_role('ROLE_USER')")
     */
    public function editAction(Request $request, $id)
    {
        $article = $this->getDoctrine()->getRepository('AppBundle:Article')->findOneBy(array(
            'id' => $id,
        ));

        $tags = array();

        foreach ($article->getTags() as $tag) {
            $tags[] = $tag->getName();
        }

        $form = $this->createFormBuilder($article)
            ->add('title', TextType::class)
            ->add('text', TextareaType::class)
            ->add('isPublic', CheckboxType::class, array(
                'label'    => 'Zobraziť tento článok verejne?',
                'required' => false,
            ))
            ->add('newImages', FileType::class, array(
                'mapped' => false,
                'required' => false,
                'constraints' => array(
                    new AllConstraint(array(
                        new File(array(
                            'maxSize' => '1024k',
                            'mimeTypes' => array(
                                'image/jpeg',
                                'image/gif',
                                'image/png',
                            ),
                            'mimeTypesMessage' => 'Prosím zvoľte validný obrázok (jpg, png, gif)',
                        ))
                    ))
                ),
                'data_class' => null,
                'multiple' => true,
            ))
            ->add('images', EntityType::class, array(
                'class' => 'AppBundle:Image',
                'choice_label' => 'url',
                'required' => false,
                'multiple' => true,
                'expanded' => true,
                'query_builder' => function (EntityRepository $er) use ($article) {
                    return $er->createQueryBuilder('u')
                        ->join('u.articles', 'a')
                        ->where('a.id = :id')
                        ->orderBy('u.id', 'ASC')
                        ->setParameter('id', $article->getId());
                },
            ))
            ->add('tags', TextType::class, array(
                'mapped' => false,
                'required' => false,
                'attr' => array(
                    'class' => 'tokenfield',
                )
            ))
            ->add('locations', EntityType::class, array(
                'class' => 'AppBundle:Location',
                'query_builder' => function(EntityRepository $er) {
                    return $er->createQueryBuilder('l')
                        ->andWhere('l.name != :name')
                        ->setParameter('name', 'NULL')
                        ->orderBy('l.name', 'ASC');
                },
                'choice_label' => 'name',
                'required' => false,
                'multiple' => true,
            ))
            ->add('save', SubmitType::class, array('label' => "Ulož článok"))
            ->getForm();

        $form->get('tags')->setData(implode(',', $tags));

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $em = $this->getDoctrine()->getManager();
            //$tagRepository = $this->getDoctrine()->getRepository('AppBundle:Tag')->findAllNamed();
            //niekde sa stratila myslienka pre ^^ ale dostalo sa to do commitu a neviem ako a preco
            $tagRepository = $this->getDoctrine()->getRepository('AppBundle:Tag');

            $article->setAddedBy($this->getUser());
            $now = new \DateTime();
            $article->setAddedTime($now);

            $em->persist($article);

            $uploadedImages = $form['newImages']->getData();

            if ($uploadedImages && !empty($uploadedImages[0])) {
                foreach ($uploadedImages as $uploadedImage) {
                    $filename = uniqid() . '.' . $uploadedImage->guessExtension();
                    $path = $this->container->getParameter('dir_images');

                    $image = new Image();
                    $image->setMime($uploadedImage->getMimeType())
                        ->setTitle($uploadedImage->getClientOriginalName())
                        ->setAddedTime($now)
                        ->setPath($path . '/' . $filename)
                        ->setUrl($this->container->getParameter('url_dir_images') .'/' . $filename);

                    $uploadedImage->move($path, $filename);

                    $info = getimagesize($path . '/' . $filename);

                    $image->setWidth($info[0])
                        ->setHeight($info[1]);

                    $article->addImage($image);

                    $em->persist($image);
                }
            }

            $selectedTags = array();

            $tagList = $form['tags']->getData();
            if ($tagList) {
                $tagList = explode(',', $tagList);

                foreach ($tagList as $item) {
                    $item = trim($item);

                    $tag = $tagRepository->findOneByName($item);

                    $exists = false;

                    if (!$tag) {
                        $tag = new Tag();
                        $tag->setName($item);
                        $em->persist($tag);
                    } else {
                        foreach ($article->getTags() as $boundTag) {
                            if ($boundTag->getName() == $tag->getName()) {
                                $exists = true;

                                break;
                            }
                        }
                    }

                    if (!$exists) {
                        $article->addTag($tag);
                    }

                    $selectedTags[$tag->getName()] = $tag;
                }
            }

            foreach ($article->getTags() as $tag) {
                if (!array_key_exists($tag->getName(), $selectedTags)) {
                    $article->removeTag($tag);
                }
            }

            $em->persist($article);

            $em->flush();

            return $this->redirectToRoute('article_detail', array("id" => $article->getId()));
        }

        $allTags = $this->getDoctrine()->getRepository('AppBundle:Tag')->findAll();

        return $this->render('article/edit.html.twig', array(
            'form' => $form->createView(),
            'allTags' => $allTags,
        ));
    }

    /**
     * @Route("/article/delete/{id}",
     *     name="article_delete",
     *     requirements={
     *         "id"="\d+"
     *     }
     * )
     *
     * @Security("has_role('ROLE_USER')")
     */
    public function deleteAction(Request $request, $id)
    {

        $article = $this->getDoctrine()->getRepository('AppBundle:Article')->findOneBy(array(
            'id' => $id,
        ));
        if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            $this->denyAccessUnlessGranted('ROLE_USER', null, 'Unable to access this page!');
        }
        $currentUser = $this->getUser();
        if (($currentUser == $article->getAddedBy()) || ($this->get('security.authorization_checker')->isGranted('ROLE_ADMIN'))){
            $em = $this->getDoctrine()->getEntityManager();
            $comments = $article->getComments();
            foreach ($comments as $comment) {
                $em->remove($comment);
            }
            $em->remove($article);
            $em->flush();
        } else {    
            throw $this->createAccessDeniedException();
        }

        return $this->render('article/delete.html.twig', array(
            'article' => $article,
        ));
    }

    /**
     * @Route("/article/delete-comment",
     *     name="delete_comment"
     * )
     *
     * @Security("has_role('ROLE_USER')")
     */
    public function deleteCommentAction(Request $request)
    {
        $data = array('result' => false);

        $commentId = $request->request->getInt('commentId', null);
        $userId = $request->request->getInt('userId', null);

        if ($this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            $currentUser = $this->getUser();
            if (($currentUser->getId() == $userId) || ($this->get('security.authorization_checker')->isGranted('ROLE_ADMIN'))){

                $comment = $this->getDoctrine()->getRepository('AppBundle:Comment')->findOneBy(array(
                    'id' => $commentId,
                ));

                $em = $this->getDoctrine()->getEntityManager();

                $em->remove($comment);
                $em->flush();
                $data = array(
                    'result' => true,
                    'id' => $commentId,
                );
            }
        }

        $response = new JsonResponse();
        $response->setData($data);
        return $response;
    }

}
