<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Location;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Location\LocationTerrain;
use AppBundle\Location\LocationContent;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Type;
use Symfony\Component\Validator\Constraints\Range;
use AppBundle\Location\Map;
use Symfony\Component\HttpFoundation\JsonResponse;

class LocationController extends Controller
{
    /**
     * @Route("/location/add", name="location_add")
     *
     * @Security("has_role('ROLE_USER')")
     */
    public function addAction(Request $request)
    {
        $location = new Location();

        $form = $this->createFormBuilder($location)
            ->add('name', TextType::class)
            ->add('posX', IntegerType::class, array(
                'constraints' => array(
                    new NotBlank(),
                    new Type(array(
                        'type' => 'integer',
                    )),
                    new Range(array(
                        'min' => 0,
                        'max' => Map::WIDTH - 1,
                    ))
                )
            ))
            ->add('posY', IntegerType::class, array(
                'constraints' => array(
                    new NotBlank(),
                    new Type(array(
                        'type' => 'integer',
                    )),
                    new Range(array(
                        'min' => 0,
                        'max' => Map::HEIGHT - 1,
                    ))
                )
            ))
            ->add('terrain', ChoiceType::class, array(
                'choices'  => LocationTerrain::getOptions(),
            ))
            ->add('content', ChoiceType::class, array(
                'choices'  => LocationContent::getOptions(),
            ))
            ->add('add', SubmitType::class, array('label' => 'Create location'))
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($location);
            $em->flush();

            return $this->redirectToRoute('location_detail', array("id" => $location->getId()));
        }

        return $this->render('location/add.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/location/list", name="location_list")
     */
    public function listAction(Request $request)
    {
        #$location = new Location();
        
        $locations = $this->getDoctrine()->getRepository('AppBundle:Location')->findAll();

        return $this->render('location/list.html.twig', array(
            'locations' => $locations,
        ));
    }

    /**
     * @Route("/location/detail/{id}",
     *     name="location_detail",
     *     requirements={
     *         "id"="\d+"
     *     }
     * )
     */
    public function detailAction(Request $request, $id)
    {
        $location = $this->getDoctrine()->getRepository('AppBundle:Location')->findOneBy(array(
            'id' => $id,
        ));

        return $this->render('location/detail.html.twig', array(
            'location' => $location,
            'terrainNames' => array_flip(LocationTerrain::getOptions()),
            'contentNames' => array_flip(LocationContent::getOptions()),
        ));
    }

    /**
     * @Route("/map",
     *     name="map",
     *     requirements={
     *         "x"="\d+",
     *         "y"="\d+"
     *     }
     * )
     */
    public function mapAction(Request $request)
    {
        $width = 30;
        $height = 20;

        $data = $this->prepareMapData(
            $width,
            $height,
            $request->query->getInt('x', null),
            $request->query->getInt('y', null)
        );

        return $this->render(
            'location/map.html.twig',
            $data
        );
    }

    protected function prepareMapData($width, $height, $centerX = null, $centerY = null)
    {
        $posX = floor(Map::WIDTH / 2);
        $posY = floor(Map::HEIGHT / 2);

        if ($centerX !== null) {
            if (($centerX >= 0) && ($centerX < Map::WIDTH)) {
                $posX = $centerX;
            }
        }

        if ($centerY !== null) {
            if (($centerY >= 0) && ($centerY < Map::HEIGHT)) {
                $posY = $centerY;
            }
        }

        $originX = max(0, min($posX - floor($width/2), (Map::WIDTH - 1) - $width));
        $originY = max(0, min($posY - floor($height/2), (Map::HEIGHT - 1) - $height));

        $locations = $this->getDoctrine()->getRepository('AppBundle:Location')->findByCoordinates($originX, $originY, $originX + $width, $originY + $height);
        $locationsByCoor = array();

        foreach ($locations as $location) {
            $locationsByCoor[$location->getPosX()][$location->getPosY()] = $location;
        }

        $colors = array(
            LocationTerrain::PLAIN => 'green',
            LocationTerrain::FOREST => 'darkgreen',
            LocationTerrain::HILLS => 'brown',
            LocationTerrain::MOUNTAIN => 'grey',
            LocationTerrain::WATER => 'blue',
            LocationTerrain::DESERT => 'yellow'
        );

        return array(
            'originX' => max(0, min($posX - floor($width/2), (Map::WIDTH - 1) - $width)),
            'originY' => max(0, min($posY - floor($height/2), (Map::HEIGHT - 1) - $height)),
            'width' => $width,
            'height' => $height,
            'posX' => $posX,
            'posY' => $posY,
            'locations' => $locationsByCoor,
            'colors' => $colors,
        );
    }

    /**
     * @Route("/map/editor",
     *     name="map_editor",
     *     requirements={
     *         "x"="\d+",
     *         "y"="\d+"
     *     }
     * )
     *
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function editMapAction(Request $request)
    {
        $width = 30;
        $height = 20;

        $formName = $this->createFormBuilder()
            ->add('posX', IntegerType::class, array(
                'label' => 'X',
                'constraints' => array(
                    new NotBlank(),
                    new Type(array(
                        'type' => 'integer',
                    )),
                    new Range(array(
                        'min' => 0,
                        'max' => Map::WIDTH - 1,
                    ))
                )
            ))
            ->add('posY', IntegerType::class, array(
                'label' => 'Y',
                'constraints' => array(
                    new NotBlank(),
                    new Type(array(
                        'type' => 'integer',
                    )),
                    new Range(array(
                        'min' => 0,
                        'max' => Map::HEIGHT - 1,
                    ))
                )
            ))
            ->add('name', TextType::class, array('label' => 'Názov'))
            ->add('add', SubmitType::class, array('label' => 'Pomenovať lokáciu'))
            ->getForm();

        $formName->handleRequest($request);

        if ($formName->isSubmitted() && $formName->isValid()) {
            $data = $formName->getData();

            $location = $this->getDoctrine()->getRepository('AppBundle:Location')->findOneBy(array(
                'posX' => $data['posX'],
                'posY' => $data['posY'],
            ));

            if (!$location) {
                $location = new Location();
                $location->setTerrain(LocationTerrain::PLAIN)
                    ->setPosX($data['posX'])
                    ->setPosY($data['posY']);
            }

            $location->setName($data['name']);

            $em = $this->getDoctrine()->getManager();
            $em->persist($location);
            $em->flush();

            return $this->redirectToRoute('map_editor', array("x" => $request->query->getInt('x', null), 'y' => $request->query->getInt('y', null)));
        }

        $formRemoveName = $this->createFormBuilder()
            ->add('posX', IntegerType::class, array(
                'label' => 'X',
                'constraints' => array(
                    new NotBlank(),
                    new Type(array(
                        'type' => 'integer',
                    )),
                    new Range(array(
                        'min' => 0,
                        'max' => Map::WIDTH - 1,
                    ))
                )
            ))
            ->add('posY', IntegerType::class, array(
                'label' => 'Y',
                'constraints' => array(
                    new NotBlank(),
                    new Type(array(
                        'type' => 'integer',
                    )),
                    new Range(array(
                        'min' => 0,
                        'max' => Map::HEIGHT - 1,
                    ))
                )
            ))
            ->add('remove', SubmitType::class, array('label' => 'Zrušiť pomenovanie'))
            ->getForm();

        $formRemoveName->handleRequest($request);

        if ($formRemoveName->isSubmitted() && $formRemoveName->isValid()) {
            $data = $formRemoveName->getData();

            $location = $this->getDoctrine()->getRepository('AppBundle:Location')->findOneBy(array(
                'posX' => $data['posX'],
                'posY' => $data['posY'],
            ));

            if ($location) {
                $location->setName(null);

                $em = $this->getDoctrine()->getManager();
                $em->persist($location);
                $em->flush();
            }

            return $this->redirectToRoute('map_editor', array("x" => $request->query->getInt('x', null), 'y' => $request->query->getInt('y', null)));
        }

        $data = $this->prepareMapData(
            $width,
            $height,
            $request->query->getInt('x', null),
            $request->query->getInt('y', null)
        );

        return $this->render(
            'location/map.html.twig',
            array_merge(
                $data,
                array(
                    'edit' => true,
                    'terrainTypes' => LocationTerrain::getOptions(),
                    'contentTypes' => LocationContent::getOptions(),
                    'formName' => $formName->createView(),
                    'formRemoveName' => $formRemoveName->createView(),
                )
            )
        );
    }

     /**
     * @Route("/map/update-location",
     *     name="update_location"
     * )
     *
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function updateLocationAction(Request $request)
    {
        $data = [];

        $posX = $request->request->getInt('x', null);
        $posY = $request->request->getInt('y', null);
        $terrain = $request->request->getInt('terrain', null);
        $content = $request->request->getInt('content', null);

        $location = $this->getDoctrine()->getRepository('AppBundle:Location')->findOneBy(array(
            'posX' => $posX,
            'posY' => $posY,
        ));

        if (!$location) {
            $location = new Location();
            $location->setPosX($posX)
                ->setPosY($posY);
        }

        if ($terrain != null) {
            $location->setTerrain($terrain);
        } else {
            if ($location->getTerrain() == null) {
                $location->setTerrain(LocationTerrain::PLAIN);
            }
        }


        if ($content != null) {
            $location->setContent($content);
            $save = true;
        }

        $em = $this->getDoctrine()->getEntityManager();

        $em->persist($location);
        $em->flush();

        return new JsonResponse($data);
    }
}

