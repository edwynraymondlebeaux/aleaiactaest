<?php

namespace AppBundle\Location;

class LocationTerrain
{
    const PLAIN = 1;
    const FOREST = 2;
    const HILLS = 3;
    const MOUNTAIN = 4;
    const WATER = 5;
    const DESERT = 6;

    public static function getOptions()
    {
        return array(
            'planina' => self::PLAIN,
            'les' => self::FOREST,
            'kopce' => self::HILLS,
            'hory' => self::MOUNTAIN,
            'voda' => self::WATER,
            'pust' => self::DESERT,
        );
    }
}

