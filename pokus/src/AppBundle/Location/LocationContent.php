<?php

namespace AppBundle\Location;

class LocationContent
{
    const CLEAN = 0;
    const MONSTER = 1;
    const TREASURE = 2;
    const TRAP = 3;
    const CITY = 4;
    const VILLAGE = 5;
    const INN = 6;
    const NPC = 7;

    public static function getOptions()
    {
        return array(
            'nic' => self::CLEAN,
            'monstrum' => self::MONSTER,
            'poklad' => self::TREASURE,
            'pasca' => self::TRAP,
            'mesto' => self::CITY,
            'dedina' => self::VILLAGE,
            'hostinec' => self::INN,
            'postava' => self::NPC,
        );
    }

}
